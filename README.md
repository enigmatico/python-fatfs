# FatFS
### FAT12 Filesystem for Python. Mostly intended for floppy images. WORK IN PROGRESS

I created this python module for two reasons. First, I need a program that is able to
create and modify floppy images on windows that is free and open source. Second, I wanted
to learn about the FAT file system. I'm sure there are modules that does this already but
I wanted to do it myself. It's also a good way to learn how to deal with file formats and
bit manipulation in Python.

On Linux it's not really necessary as you can create any filesystems you want on a file and
then mount it. But it's always a nice thing to have anyways.

Right now it only consists of two files: FatFS.py (with the main FatFSclass) and Utils.py
(With utils and stuff).

To use the FatFS class, copy the files into your project and then import it. Example:

```python
from FatFS import FatFS, FloppyTypes

fs = FatFS("testfat/newfloppy.img",True, FloppyTypes.FLOP144M)
fs.writeFile("testfat/copy/TEST.GIF")
fs.close()
```

To open an existing floppy image and extract files from it:

```python
from FatFS import FatFS

fs = FatFS("testfat/floppy.img")

fs.extract("/SUBDIR/FILE.TXT", "FILE.TXT")
fs.close()
```

FatFS accepts the following parameters:

> FatFS(path_to_fs: str, createnew: bool = False, createfloppy: FloppyTypes = FloppyTypes.FLOP144M)

If createnew is set to True, a new formatted media will be created into the path specified by path_to_fs. createFloppy specifies
the media type (One of FloppyTypes.FLOP144M or FloppyTypes.FLOP720k). If createnew is set to False, it will instead try to open
an existing filesystem in the path specified in path_to_fs, and will ignore createFloppy.

Using the extract function, you can extract files from the floppy disk.

> extract(self, path: str, output: str) -> bool

path specifies the path of the file to be extracted from the disk. output specifies where it will be extracted (in your computer).

To write files into a disk, use writeFile:

> writeFile(self, file: str, path: str = "/", overwrite: bool = False) -> bool

Example:

```python
fs.writeFile("./testfat/copy/TEST2.GIF", "/TEST")
```

Will copy the file ./testfat/copy/TEST2.GIF into /TEST (inside of the disk). The parameter 'file' must contain a valid path to the file you want to write into the disk.

Use mkdir to create directories and subdirectories:

> mkdir(self, path: str) -> bool

Example:

```python
fs.mkdir("/TEST/SUBDIR")
```

Will create the directory "SUBDIR" inside of "/TEST". "/TEST" must exist in order for this function to work.

To delete both files AND folders, use delFile

> delFile(self, file: str, path: str = "/", isDirectory: bool = False) -> bool

Example:

```python
fs.delFile("TEST3.GIF", "/TEST/SUBDIR")
```

Will delete the file TEST3.GIF that is in the "/TEST/SUBDIR" directory. If 'isDirectory' is set to True, file must
contain the name of a directory instead of a file. The function will then delete ALL FILES AND SUBDIRECTORIES
RECURSIVELY INSIDE OF THE SPECIFIED DIRECTORY, and then proceed to remove the directory.

To copy a file (and it's contents) to another directory, use copyFile.

> copyFile(self, file: str, targetpath: str, path: str = "/", isDir = False) -> bool

Example:

```python
fs.copyFile("TEST3.GIF", "/", "/TEST/SUBDIR")
```

This will copy the file "/TEST/SUBDIR/TEST3.GIF" into the root directory ("/").
The parameter isDir is NOT FULLY IMPLEMENTED AND SHALL NOT BE USED YET. For now, you must leave it as "False".

Similarly, to move a file from one directory to another, you can use the moveFile function.

> moveFile(self, file: str, targetpath: str, path: str = "/") -> bool

Example:

```python
fs.moveFile("TEST2.GIF", "/TEST/SUBDIR", "/TEST")
```

This will move the file "/TEST/TEST2.GIF" into the folder "/TEST/SUBDIR".

# THIS IS A WORK IN PROGRESS