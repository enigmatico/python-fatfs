#############################################################################
#                                                                           #
#                           F a t F S . p y                                 #
#                                                                           #
#             A python class to manipulate FAT 12 filesystems.              #
#   --------------------------------------------------------------------    #
#   Developed by: Enigmatic0                                                #
#   Date: May 20, 2022                                                      #
#   Last update: Jun 05, 2022                                               #
#   Status: WIP                                                             #
#   Python version: 3.9+                                                    #
#                                                                           #
#############################################################################

from struct import Struct, calcsize
from math import floor, ceil
from io import BytesIO
from enum import Enum
from Utils import FileHandler, bitCheck, FileMode
import os
import sys
from datetime import datetime

# List of valid FAT characters for names and extensions
FATVALIDCHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 $%-_@~`(){}^#&+,;=[]."

# Boot sector struct format.
BOOT_SECTOR_FORMAT = "<3x8sHBHBHHBHHH4xI2xBI11s8s62x"
# Directory Entry struct format (FAT12).
DIRENTRY_FAT12_FORMAT = "<8s3sBBBHHHHHHHI"

# (Enum) Floppy types. The values represent the max capacity (in bytes) ignoring the FS.
class FloppyTypes(Enum):
    FLOP720k = 737280
    FLOP144M = 1474560

# (Enum) FAT values for the FAT table.
class FatEntry(Enum):
    FREE = 0x00
    RESERVED = 0xFF0 # Check if NEXT >= RESERVED AND NEXT <= RESERVED 2 (0xFF0 - 0xFF6)
    RESERVED2 = 0xFF6
    BAD_CLUSTER = 0xFF7
    LAST_CLUSTER = 0xFF8 # Check if NEXT >= LAST_CLUSTER (0xFF8 - 0xFFF)

# (Enum) Directory entry class. This correspond to the first byte
# of the file name and determines if the file has been deleted (free) or
# if this is the final entry in the directory.
class DirEntryClass(Enum):
    FREE = 0xE5
    FINAL = 0x00

# (Enum) Media descriptor values. We only support 720k and 144M though.
class MediaDescriptor(Enum):
    FLOP640k = 0xFb
    FLOP320k = 0xFa
    FLOP720k = 0xF9
    FLOP144M = 0xF0
    RAMDISK = 0xFA
    HARDDISK = 0xF8

# (Enum) Some known OEM names.
# ENIGMATX references this program.
class OEMNames(Enum):
    IBM33 = "IBM 3.3 "
    IBM20 = "IBM 20.0"
    DOS5 = "MSDOS5.0"
    DOS6 = "MSDOS6.0"
    WIN4 = "MSWIN4.0"
    DEFAULT = "ENIGMATX"

# (Enum) (Unused, might remove) FAT filesystem.
class FatClass(Enum):
    INVALID = 0
    FAT12 = 1
    FAT16 = 2
    FAT32 = 4
    EXFAT = 8

# (Enum) FAT file attributes. Those are the attributes for the files and directories.
class FATFileAttributes(Enum):
    READONLY = 0x01
    HIDDEN = 0x02
    SYSTEM = 0x04
    LABEL = 0x08
    DIRECTORY = 0x10
    ARCHIVE = 0x20
    UNUSED = 0x40
    INVALID = 0x80 # This bit is also unused

# Class that represents a FAT12 Directory Entry.
class Fat12DirectoryEntry:
    # Class constructor
    # bsbin: Tuple with a direntry previously read using struct.unpack (or none for an empty FAT12DirectoryEntry)
    def __init__(self, bsbin: tuple or None):
        if bsbin is not None:
            try:
                # If the filename is invalid, most certainly this is an invalid entry.
                if not FatUtils.isValidDirName(bsbin[0].decode('utf-8').strip()) or not FatUtils.isValidDirName(bsbin[1].decode('utf-8').strip()):
                    self.invalid = True
                else:
                    self.invalid = False
            except UnicodeDecodeError as exc:
                # No weird characters allowed in the name
                self.invalid = True
            if FatUtils.isFree(bsbin[0]):
                # This entry was freed (deleted)
                self.free = True
                self.end = False
            elif FatUtils.isFinal(bsbin[0]):
                # This is the final entry in the list
                self.free = False
                self.end = True
            else:
                # This is just another directory entry
                self.free = False
                self.end = False
            # Load all the parameters from the structure tuple
            # (This is the FATEntry data structure from the FAT12)
            self.fileName = bsbin[0][0:8].decode('utf-8').strip() if not self.invalid else ""
            self.fileExtension = bsbin[1].decode('utf-8').strip() if not self.invalid else ""
            self.attributes = FatUtils.attrDecode(bsbin[2])
            self.case = bsbin[3]
            self.creationTimeMS = bsbin[4]
            # Ignore the date for time objects, and the time for date objects.
            self.creationTime = FatUtils.decodeTime(bsbin[5])
            self.creationDate = FatUtils.decodeDate(bsbin[6])
            self.lastAccessDate = FatUtils.decodeDate(bsbin[7])
            self.startingClusterHW = bsbin[8] # Unused in FAT12
            self.lastWriteTime = FatUtils.decodeTime(bsbin[9])
            self.lastWriteDate = FatUtils.decodeDate(bsbin[10])
            self.firstLogicalCluster = bsbin[11]
            self.size = bsbin[12]
            # These are not part of the FAT12 directory structure.
            # Full datetime objects with the creation and last write date and time
            self.creationDateTime = FatUtils.decodeDateTime(bsbin[6], bsbin[5])
            self.lastWriteDateTime = FatUtils.decodeDateTime(bsbin[10], bsbin[9])
        else:
            # Empty FAT12 direntry
            self.invalid = False
            self.free = True
            self.end = False
            self.fileName = [0x00] * 8
            self.fileExtension = [0x00] * 3
            self.attributes = []
            self.case = 0
            self.creationTimeMS = 0
            self.creationTime = 0
            self.creationDate = 0
            self.lastAccessDate = 0
            self.startingClusterHW = 0 # Unused in FAT12
            self.lastWriteTime = 0
            self.lastWriteDate = 0
            self.firstLogicalCluster = 0
            self.size = 0
            self.creationDateTime = datetime(1980,1,1,0,0,0,0)
            self.lastWriteDateTime = datetime(1980,1,1,0,0,0,0)

    # Encode the directory entry attributes into a single byte.
    def encodeAttributes(self) -> int:
        attribs = 0
        for attr in self.attributes:
            # Also ensure it's one byte by masking it. I don't trust python with bit manipulation.
            attribs = (attribs | attr.value)&0xFF
        return attribs

    # "Serializes" this class into a tuple. Useful for packing later.
    def getDataTuple(self) -> tuple:
        encodedattr = self.encodeAttributes()
        fileName = self.fileName
        fileExtension = self.fileExtension
        if len(fileName) < 8:
            fileName += (" " * (8-len(fileName)))
        if len(fileExtension) < 3:
            fileExtension += (" " * (3-len(fileExtension)))
        return (
            fileName.encode('utf-8'),
            fileExtension.encode('utf-8'),
            encodedattr,
            self.case,
            self.creationTimeMS,
            FatUtils.encodeTime(self.creationTime) if type(self.creationTime) is not int else self.creationTime,
            FatUtils.encodeDate(self.creationDate) if type(self.creationDate) is not int else self.creationDate,
            FatUtils.encodeDate(self.lastAccessDate) if type(self.lastAccessDate) is not int else self.lastAccessDate,
            self.startingClusterHW,
            FatUtils.encodeTime(self.lastWriteTime) if type(self.lastWriteTime) is not int else self.lastWriteTime,
            FatUtils.encodeDate(self.lastWriteDate) if type(self.lastWriteDate) is not int else self.lastWriteDate,
            self.firstLogicalCluster,
            self.size
        )

    def debugPrint(self):
        print(f"    filename:   {self.fileName.encode(encoding='UTF-8')}")
        print(f"    extension:   {self.fileExtension.encode(encoding='UTF-8')}")
        print(f"    attributes:   {self.encodeAttributes()}:")
        for attr in self.attributes:
            print(f"        {attr}")
        print(f"    case:   {self.case}:")
        print(f"    creationTimeMS:   {self.creationTimeMS}:")
        print(f"    creationTime:   {self.creationTime}:")
        print(f"    creationDate:   {self.creationDate}:")
        print(f"    lastAccessDate:   {self.lastAccessDate}:")
        print(f"    startingClusterHW:   {self.startingClusterHW}:")
        print(f"    lastWriteTime:   {self.lastWriteTime}:")
        print(f"    lastWriteDate:   {self.lastWriteDate}:")
        print(f"    firstLogicalCluster:   {self.firstLogicalCluster}:")
        print(f"    size:   {self.size}")
        


# Functions related to the FAT filesystem
class FatUtils:
    # Check if a directory entry name is valid
    def isValidDirName(name: str)->bool:
        # Check if the name contains only valid characters, including characters
        # above ASCII value 127.
        return not (any((x not in FATVALIDCHARACTERS and ord(x) < 127) for x in name))

    # Check if a directory entry name is marked as free.
    # dirname must be a bytes like object (bytearray or BytesIO)
    def isFree(dirname: bytes) -> bool:
        return dirname[0] == DirEntryClass.FREE.value

    # Check if a directory entry name is marked as final
    # dirname must be a bytes like object (bytearray or BytesIO)
    def isFinal(dirname: bytes) -> bool:
        return dirname[0] == DirEntryClass.FINAL.value

    # Creates a list of file attributes from the directory entry attribute bitflags
    # attrs: int with the byte value
    def attrDecode(attrs: int) -> list:
        attributes = []
        for i in range(1,8):
            if bitCheck(attrs, i):
                attributes.append(FATFileAttributes(2**(i-1)))
        return attributes

    # Decode a FAT time value into a time object
    def decodeTime(value: int) -> datetime:
        defaultdt = datetime(1980,1,1, 0, 0, 0)
        seconds = (value & 0x001F)<<1
        minutes = (value & 0x07E0) >> 5
        hours = (value & 0xF800) >> 11
        if seconds > 59 or minutes > 59 or hours > 23:
            return defaultdt
        return datetime(1980,1,1, hours, minutes, seconds)

    # Encode a time object into a FAT time value
    def encodeTime(timev: datetime) -> int:
        seconds = timev.second >>1
        minutes = timev.minute << 5
        hours = timev.hour << 11
        return seconds + minutes + hours

    # Decode a FAT date into a datetime object
    def decodeDate(value: int) -> datetime:
        defaultdt = datetime(1980,1,1, 0, 0, 0)
        day = (value & 0x001F)
        month = (value & 0x01E0) >> 5
        year = ((value & 0xFE00) >> 9) + 1980
        if day > 31 or day < 1 or month > 12 or month < 1 or year > 2107 or year < 1980:
            return defaultdt
        return datetime(year, month, day)

    # Encode a date object into a FAT date
    def encodeDate(datet: datetime) -> int:
        day = datet.day
        month = datet.month << 5
        year = (datet.year - 1980) << 9
        return day + month + year

    # Decodes a FAT date and time into a single datetime
    def decodeDateTime(datet: int, timet: int) -> tuple:
        defaultdt = datetime(1980,1,1, 0, 0, 0)
        seconds = (timet & 0x001F)<<1
        minutes = (timet & 0x07E0) >> 5
        hours = (timet & 0xF800) >> 11
        day = (datet & 0x001F)
        month = (datet & 0x01E0) >> 5
        year = ((datet & 0xFE00) >> 9) + 1980
        if seconds > 59 or minutes > 59 or hours > 23:
            return defaultdt
        if day > 31 or day < 1 or month > 12 or month < 1 or year > 2107 or year < 1980:
            return defaultdt
        return datetime(year, month, day, hours, minutes, seconds)

    # Encodes a datetime object into a tuple of FAT time and date (tuple: (time, date))
    def encodeDateTime(datet: datetime) -> tuple:
        seconds = datet.second >>1
        minutes = datet.minute << 5
        hours = datet.hour << 11
        day = datet.day
        month = datet.month << 5
        year = (datet.year - 1980) << 9
        return((seconds+minutes+hours),(day+month+year))

    # Get an integer with the current 100th of a second.
    # HACK: Not exactly a 100th of a second but, close enough.
    def getCurrent100thSecondByte(date: datetime = None) -> int:
        if date is None:
            return round(round(datetime.now().microsecond / 100000, 1))
        else:
            return round(round(date.microsecond / 100000, 1))

    # Generate a new volume ID
    # This is how many systems do it (according to https://www.digital-detective.net/documents/Volume%20Serial%20Numbers.pdf)
    def genVolumeID(datet: datetime = datetime.now()) -> int:
        # (Month at the higher byte, day at the lower) + (Second at the higher, ms at the lower)
        vol_high = ((datet.month << 8) + datet.day) + round(((datet.second << 8) + round(datet.microsecond/100000, 1)))
        # (Hour at the higher byte, minute at the lower) + year (2 bytes)
        vol_low = ((datet.hour << 8) + datet.minute) + datet.year
        # (vol_high-vol_low)
        vol_id = (vol_high << 16) + vol_low
        # Ensure that the value is 32 bits (trim it if it's bigger)
        if len(bin(vol_id)) - 2 > 32:
            vol_id = vol_id & 0xFFFFFFFF # Ensure it's 32-bits
        return vol_id

    # Return the number of clusters required to store a file
    def calculateRequiredClusters(filesize: int, bytesPerSector: int, sectorsPerCluster: int) -> int:
        return int(ceil((filesize / bytesPerSector) * sectorsPerCluster))

    def getCurrentDirectory(firstLogicalCluster: int, date: datetime = None):
        if date is None:
            date = datetime.now()
        dirent = Fat12DirectoryEntry(None)
        dirent.fileName = ".       "
        dirent.fileExtension = "   "
        dirent.attributes = [ FATFileAttributes.DIRECTORY ]
        dirent.case = 0
        dirent.creationTimeMS = FatUtils.getCurrent100thSecondByte(date)
        dirent.creationDate = FatUtils.encodeDate(date)
        dirent.creationTime = FatUtils.encodeTime(date)
        dirent.lastAccessDate = FatUtils.encodeDate(date)
        dirent.lastWriteDate = FatUtils.encodeDate(date)
        dirent.lastWriteTime = FatUtils.encodeTime(date)
        dirent.startingClusterHW = 0
        dirent.firstLogicalCluster = firstLogicalCluster
        dirent.size = 0
        return dirent
        
    # TODO: Merge with getCurrentDirectory into getSpecialDirectory
    def getParentDirectory(firstLogicalCluster: int, date: datetime = None):
        if date is None:
            date = datetime.now()
        dirent = Fat12DirectoryEntry(None)
        dirent.fileName = "..      "
        dirent.fileExtension = "   "
        dirent.attributes = [ FATFileAttributes.DIRECTORY ]
        dirent.case = 0
        dirent.creationTimeMS = FatUtils.getCurrent100thSecondByte(date)
        dirent.creationDate = FatUtils.encodeDate(date)
        dirent.creationTime = FatUtils.encodeTime(date)
        dirent.lastAccessDate = FatUtils.encodeDate(date)
        dirent.lastWriteDate = FatUtils.encodeDate(date)
        dirent.lastWriteTime = FatUtils.encodeTime(date)
        dirent.startingClusterHW = 0
        dirent.firstLogicalCluster = firstLogicalCluster
        dirent.size = 0
        return dirent


# Class that represents the boot sector of the FAT
# bsbin: BytesIO with the boot sector information in binary, or None for an empty FatBootSector
class FatBootSector:
    def __init__(self, bsbin: BytesIO or None):
        self.bsstruct = Struct(BOOT_SECTOR_FORMAT)
        if bsbin is not None:
            data = self.bsstruct.unpack(bsbin)
            # Those are the standard boot sector fields
            self.oem = data[0].decode('utf-8').strip()
            self.bytesPerSector = data[1]
            self.sectorsPerCluster = data[2]
            self.reservedSectors = data[3]
            self.fats = data[4]
            self.rootEntries = data[5]
            self.totalSectors = data[6]
            self.mediaDescriptor = data[7]
            self.sectorsPerFat = data[8]
            self.sectorsPerTrack = data[9]
            self.heads = data[10]
            self.sectorCount = data[11]
            self.bootSignature = data[12]
            self.volumeID = data[13]
            self.label = data[14].decode('utf-8').strip()
            self.fileSystem = data[15].decode('utf-8').strip()

            # Those are precomputed calculations (not part of the FAT itself) 
            self.update()
        else:
            # Generate an empty FatBootSector
            self.oem = None
            self.bytesPerSector = None
            self.sectorsPerCluster = None
            self.reservedSectors = None
            self.fats = None
            self.rootEntries = None
            self.totalSectors = None
            self.mediaDescriptor = None
            self.sectorsPerFat = None
            self.sectorsPerTrack = None
            self.heads = None
            self.sectorCount = None
            self.bootSignature = None
            self.volumeID = None
            self.label = None
            self.fileSystem = None

    # Precomputed values (not a part of the bootsector itself)
    def update(self):
        # Total size (in sectors) of the root directory
        self.rootTotalSectors = ceil((calcsize(DIRENTRY_FAT12_FORMAT) * self.rootEntries)/self.bytesPerSector)
        # Total size (in bytes) of the root directory
        self.rootTotalSize = int(self.rootTotalSectors * self.bytesPerSector)
        # The cluster of the root directory (Should be 19 for FAT12 since it's fixed)
        self.rootDirCluster = floor(self.sectorsPerCluster + (self.sectorsPerFat * self.fats)) / self.sectorsPerCluster
        # The offset of the root directory (Should also be fixed for FAT12)
        self.rootDirOffset = self.bytesPerSector + (self.sectorsPerFat * self.bytesPerSector) * self.fats
        # The offset of the first FAT
        self.firstFatOffset = self.bytesPerSector
        # The offset of the second FAT
        self.secondFatOffset = (self.sectorsPerFat * self.bytesPerSector) + self.firstFatOffset
        # The sector of the first FAT (always 1 lol)
        self.firstFATSector = 1
        # The size of one FAT in bytes
        self.totalFATSize = self.sectorsPerFat * self.bytesPerSector
        # Total FAT entries per FAT
        self.fatEntries = self.totalSectors * self.sectorsPerCluster
        # The total size of the FAT section
        self.totalFATAreaSize = self.totalFATSize * self.fats
        # The offset of the data section
        self.dataStartOffset = floor(self.reservedSectors + (self.fats * self.sectorsPerFat) + ((self.rootEntries*32)/self.bytesPerSector))

    # "Serializes" the class instance into a tuple with all the values. Useful for packing later.
    def getTuple(self) -> tuple:
        return (
        self.oem.encode(encoding='UTF-8'),
        self.bytesPerSector,
        self.sectorsPerCluster,
        self.reservedSectors,
        self.fats,
        self.rootEntries,
        self.totalSectors,
        self.mediaDescriptor,
        self.sectorsPerFat,
        self.sectorsPerTrack,
        self.heads,
        self.sectorCount,
        self.bootSignature,
        self.volumeID,
        self.label.encode(encoding='UTF-8'),
        self.fileSystem.encode(encoding='UTF-8')
        )

    # Return the binary data of the boot sector.
    # readonly: If set to True, returns an immutable bytearray (BytesIO)
    def getBinaryData(self, readonly = False) -> bytearray or BytesIO:
        data = bytearray(self.bytesPerSector)

        # Boot sector structure
        # HACK: FIXME: Make a "memcopy" sort of function or find one to do this correctly.
        # This appends data, which is why I have to substract in the next one
        data[0:self.bsstruct.size] = bytearray(self.bsstruct.pack(*self.getTuple()))

        # Binary code (JMP 003E NOP)
        data[0] = 0xEB
        data[1] = 0x3C
        data[2] = 0x90

        # Very huge bootstrap code.
        # HACK: FIXME: Make a "memcopy" sort of function or find one to do this correctly.
        # I don't even know how does this work but it does. Also maybe copy from an external
        # boot loader binary file instead of hard-coding it?
        data[self.bsstruct.size-65:self.totalFATAreaSize] = [
            0x20, 0x20, 0x20, 0x33, 0xC9, 0x8E,
            0xD1, 0xBC, 0xF0, 0x7B, 0x8E, 0xD9, 0xB8, 0x00, 0x20, 0x8E, 0xC0, 0xFC, 0xBD,
            0x00, 0x7C, 0x38, 0x4E, 0x24, 0x7D, 0x24, 0x8B, 0xC1, 0x99, 0xE8, 0x3C, 0x01,
            0x72, 0x1C, 0x83, 0xEB, 0x3A, 0x66, 0xA1, 0x1C, 0x7C, 0x26, 0x66, 0x3B, 0x07,
            0x26, 0x8A, 0x57, 0xFC, 0x75, 0x06, 0x80, 0xCA, 0x02, 0x88, 0x56, 0x02, 0x80,
            0xC3, 0x10, 0x73, 0xEB, 0x33, 0xC9, 0x8A, 0x46, 0x10, 0x98, 0xF7, 0x66, 0x16,
            0x03, 0x46, 0x1C, 0x13, 0x56, 0x1E, 0x03, 0x46, 0x0E, 0x13, 0xD1, 0x8B, 0x76,
            0x11, 0x60, 0x89, 0x46, 0xFC, 0x89, 0x56, 0xFE, 0xB8, 0x20, 0x00, 0xF7, 0xE6,
            0x8B, 0x5E, 0x0B, 0x03, 0xC3, 0x48, 0xF7, 0xF3, 0x01, 0x46, 0xFC, 0x11, 0x4E,
            0xFE, 0x61, 0xBF, 0x00, 0x00, 0xE8, 0xE6, 0x00, 0x72, 0x39, 0x26, 0x38, 0x2D,
            0x74, 0x17, 0x60, 0xB1, 0x0B, 0xBE, 0xA1, 0x7D, 0xF3, 0xA6, 0x61, 0x74, 0x32,
            0x4E, 0x74, 0x09, 0x83, 0xC7, 0x20, 0x3B, 0xFB, 0x72, 0xE6, 0xEB, 0xDC, 0xA0,
            0xFB, 0x7D, 0xB4, 0x7D, 0x8B, 0xF0, 0xAC, 0x98, 0x40, 0x74, 0x0C, 0x48, 0x74,
            0x13, 0xB4, 0x0E, 0xBB, 0x07, 0x00, 0xCD, 0x10, 0xEB, 0xEF, 0xA0, 0xFD, 0x7D,
            0xEB, 0xE6, 0xA0, 0xFC, 0x7D, 0xEB, 0xE1, 0xCD, 0x16, 0xCD, 0x19, 0x26, 0x8B,
            0x55, 0x1A, 0x52, 0xB0, 0x01, 0xBB, 0x00, 0x00, 0xE8, 0x3B, 0x00, 0x72, 0xE8,
            0x5B, 0x8A, 0x56, 0x24, 0xBE, 0x0B, 0x7C, 0x8B, 0xFC, 0xC7, 0x46, 0xF0, 0x3D,
            0x7D, 0xC7, 0x46, 0xF4, 0x29, 0x7D, 0x8C, 0xD9, 0x89, 0x4E, 0xF2, 0x89, 0x4E,
            0xF6, 0xC6, 0x06, 0x96, 0x7D, 0xCB, 0xEA, 0x03, 0x00, 0x00, 0x20, 0x0F, 0xB6,
            0xC8, 0x66, 0x8B, 0x46, 0xF8, 0x66, 0x03, 0x46, 0x1C, 0x66, 0x8B, 0xD0, 0x66,
            0xC1, 0xEA, 0x10, 0xEB, 0x5E, 0x0F, 0xB6, 0xC8, 0x4A, 0x4A, 0x8A, 0x46, 0x0D,
            0x32, 0xE4, 0xF7, 0xE2, 0x03, 0x46, 0xFC, 0x13, 0x56, 0xFE, 0xEB, 0x4A, 0x52,
            0x50, 0x06, 0x53, 0x6A, 0x01, 0x6A, 0x10, 0x91, 0x8B, 0x46, 0x18, 0x96, 0x92,
            0x33, 0xD2, 0xF7, 0xF6, 0x91, 0xF7, 0xF6, 0x42, 0x87, 0xCA, 0xF7, 0x76, 0x1A,
            0x8A, 0xF2, 0x8A, 0xE8, 0xC0, 0xCC, 0x02, 0x0A, 0xCC, 0xB8, 0x01, 0x02, 0x80,
            0x7E, 0x02, 0x0E, 0x75, 0x04, 0xB4, 0x42, 0x8B, 0xF4, 0x8A, 0x56, 0x24, 0xCD,
            0x13, 0x61, 0x61, 0x72, 0x0B, 0x40, 0x75, 0x01, 0x42, 0x03, 0x5E, 0x0B, 0x49,
            0x75, 0x06, 0xF8, 0xC3, 0x41, 0xBB, 0x00, 0x00, 0x60, 0x66, 0x6A, 0x00, 0xEB,
            0xB0, 0x4E, 0x54, 0x4C, 0x44, 0x52, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x0D,
            0x0A, 0x4E, 0x54, 0x4C, 0x44, 0x52, 0x20, 0x69, 0x73, 0x20, 0x6D, 0x69, 0x73,
            0x73, 0x69, 0x6E, 0x67, 0xFF, 0x0D, 0x0A, 0x44, 0x69, 0x73, 0x6B, 0x20, 0x65,
            0x72, 0x72, 0x6F, 0x72, 0xFF, 0x0D, 0x0A, 0x50, 0x72, 0x65, 0x73, 0x73, 0x20,
            0x61, 0x6E, 0x79, 0x20, 0x6B, 0x65, 0x79, 0x20, 0x74, 0x6F, 0x20, 0x72, 0x65,
            0x73, 0x74, 0x61, 0x72, 0x74, 0x0D, 0x0A, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0xAC, 0xBF, 0xCC, 0x55, 0xAA
        ]
        return data if not readonly else BytesIO(data)

# Main class
class FatFS:
    # Constructor: path_to_fs: Path to a FAT12 file system.
    # path_to_fs: Path to a floppy image or physical floppy disk (If that's still possible in 202x)
    # createnew: If set to True, a new floppy image with a new FAT12 filesystem will be created.
    # createfloppy: FloppyTypes. Can be either FloppyTypes.FLOP720k or FloppyTypes.FLOP144M.
    # createfloppy is ignored if createnew is False
    def __init__(self, path_to_fs: str, createnew: bool = False, createfloppy: FloppyTypes = FloppyTypes.FLOP144M):
        # Global time
        self.global_time = datetime.now()
        try:
            # Direntry struct
            self.__dirstruct = Struct(DIRENTRY_FAT12_FORMAT)
            if not createnew:
                self.__fs = FileHandler(path_to_fs)
                # Empty volume label
                self.vol_label = "NO LABEL"
                # Load the boot sector
                self.bootsector = FatBootSector(self.__fs.readandrewind(calcsize(BOOT_SECTOR_FORMAT)))
                # Load the FAT
                self.__fs.seek(self.bootsector.firstFatOffset)
                self.__fattabledata = bytearray(self.__fs.read(self.bootsector.sectorsPerFat * self.bootsector.bytesPerSector * self.bootsector.fats, False))
                # Filesystem cache
                # __fscache list: tuple(name (str), directory (str), full_path (str), is_directory (bool),
                #                       direntry (FAT12DirectoryEntry))
                self.__fscache = []
                # Build the filesystem cache (for quick searching)
                self.__buildcache(self.bootsector.rootDirCluster, "/")
                avail = self.__computeFreeClusters()
            else:
                written = 0
                # Create an empty system
                self.__fs = FileHandler(path_to_fs, FileMode.WRITEREADBYTES, True)
                # Temporary empty label
                self.vol_label = "NO LABEL"
                # Create a new boot sector for the specified floppy type
                self.floppytype = createfloppy
                self.bootsector = self.__makeFAT12BootSector(self.floppytype)
                # Create a new FAT
                self.__fattabledata = bytearray(self.bootsector.sectorsPerFat * self.bootsector.bytesPerSector * self.bootsector.fats)
                # Create an empty filesystem cache
                self.__fscache = []

                # Write the file system
                buf_out = self.bootsector.getBinaryData()
                
                # Write the boot sector
                written += self.__fs.write(buf_out)
                # Write the FAT table. The first two clusters are reserved
                self.__fattabledata[0] = 0xF0
                self.__fattabledata[1] = 0xFF
                self.__fattabledata[2] = 0xFF

                # Offset of the second fat relative to our FAT table
                fat2 = self.bootsector.sectorsPerFat * self.bootsector.bytesPerSector
                
                # Also on the second fat
                self.__fattabledata[fat2] = 0xF0
                self.__fattabledata[fat2 + 1] = 0xFF
                self.__fattabledata[fat2 + 2] = 0xFF

                written += self.__fs.write(self.__fattabledata)

                # Write the Root directory (Empty).
                # The root sector usually occuppies sectors 19 to 32
                #sectors_for_root = ceil((calcsize(DIRENTRY_FAT12_FORMAT) * self.bootsector.rootEntries)/self.bootsector.bytesPerSector)
                #rootsize = int(sectors_for_root * self.bootsector.bytesPerSector)
                root = bytearray(self.bootsector.rootTotalSize)

                # Add the volume label to the root directory
                vol_label = (str.encode("ENIGMATX", encoding="UTF-8"), bytearray([0x20] * 3),
                            FATFileAttributes.LABEL.value, 0, FatUtils.getCurrent100thSecondByte(), FatUtils.encodeTime(self.global_time),
                            FatUtils.encodeDate(self.global_time), FatUtils.encodeDate(self.global_time), 0,
                            FatUtils.encodeTime(self.global_time), FatUtils.encodeDate(self.global_time),0, 0)

                vol_label = self.__dirstruct.pack(*vol_label)
                # HACK: Again, this will append, not replace bytes to the array.
                # Since the data area is empty and is written later the excess will
                # be replaced later anyways. Maybe FIXME with a memcpy sort of funcion?
                root[0:calcsize(DIRENTRY_FAT12_FORMAT)] = vol_label

                # Write the root directory
                written += self.__fs.write(root)

                # Write the data area
                written += self.__fs.write(bytearray([0xF6] * (self.floppytype.value - written)))
        except Exception as exc:
            raise exc

    # (Private) Creates a new boot sector.
    # size: FloppyTypes. Used to determine the size of the disk in bytes. Either FloppyTypes.FLOP720k or FloppyTypes.FLOP144M
    def __makeFAT12BootSector(self, size: FloppyTypes = FloppyTypes.FLOP144M) -> FatBootSector:
        bootsec = FatBootSector(None)
        if size == FloppyTypes.FLOP720k:
            bootsec.oem = OEMNames.DOS5.value
            bootsec.bytesPerSector = 512
            bootsec.sectorsPerCluster = 2
            bootsec.reservedSectors = 1
            bootsec.fats = 2
            bootsec.rootEntries = 112
            bootsec.totalSectors = 1440
            bootsec.mediaDescriptor = MediaDescriptor.FLOP720k.value
            bootsec.sectorsPerFat = 3
            bootsec.sectorsPerTrack = 9
            bootsec.heads = 2
            bootsec.sectorCount = 0
            bootsec.bootSignature = 41
            bootsec.volumeID = FatUtils.genVolumeID(self.global_time)
            bootsec.label = "NO NAME    "
            bootsec.fileSystem = "FAT12   "
            bootsec.update()
            return bootsec
        elif size == FloppyTypes.FLOP144M:
            bootsec.oem = OEMNames.DOS5.value
            bootsec.bytesPerSector = 512
            bootsec.sectorsPerCluster = 1
            bootsec.reservedSectors = 1
            bootsec.fats = 2
            bootsec.rootEntries = 224
            bootsec.totalSectors = 2880
            bootsec.mediaDescriptor = MediaDescriptor.FLOP144M.value
            bootsec.sectorsPerFat = 9
            bootsec.sectorsPerTrack = 18
            bootsec.heads = 2
            bootsec.sectorCount = 0
            bootsec.bootSignature = 41
            bootsec.volumeID = FatUtils.genVolumeID(self.global_time)
            bootsec.label = "NO NAME    "
            bootsec.fileSystem = "FAT12   "
            bootsec.update()
            return bootsec
        else:
            return None

    # (Private) (Unused) Converts an absolute (disk) logical cluster into an offset
    def __clusterToOffset(self, cluster: int) -> int:
        return((self.bootsector.sectorsPerCluster * cluster) * self.bootsector.bytesPerSector)

    # (Private) Converts a data (data section) cluster into an offset
    def __fileClusterToOffset(self, cluster: int) -> int:
        return (self.bootsector.dataStartOffset + ((cluster-2)*self.bootsector.sectorsPerCluster)) * self.bootsector.bytesPerSector

    # (Private) Read the FAT to obtain the next cluster
    def __getNextCluster(self, cluster: int) -> int:
        # FAT12 only
        # Align the offset to 3/2 ((cluster / 2) + cluster). We want either the odd, or the even byte.
        # The last byte is part of the value so we don't read from there.
        offset = (cluster >> 0x0001) + cluster

        # Read 2 bytes from the FAT starting at offset.
        fatent = int.from_bytes(self.__fattabledata[offset:offset+2], byteorder=sys.byteorder)

        # Decode the cluster value depending on wether cluster is odd or even
        # FAT12 packs these two 12-bit values into three bytes in the following order:
        # (Being abc and XYZ two different values packed in little endian order):
        #       bc Za XY
        # When decoding, we read from the first (odd) byte as a WORD (short) for the first value,
        # or from the second (even) byte also as a WORD:
        #   odd: Za bc
        #   even: XY Za
        # So, to recover the odd value, we just mask the higher 4-bits:
        #   Za bc & 0x0fff = 0a bc
        # And we shift the even value 4 bits to the right to get it's value:
        #   XY Za >> 4 = 0X YZ
        #
        # Simple enough!

        if cluster & 0x0001 == 1:
            return fatent >> 0x0004
        else:
            return fatent & 0x0fff

    # (Private) Update the next cluster value for the current cluster
    # cluster: current cluster
    # value: next cluster (Can be an integer or a FatEntry)
    def __updateCluster(self, cluster: int, value: int or FatEntry) -> int:
        if type(value) is FatEntry:
            # Just get the value of the FatEntry enum
            value = value.value
        if cluster >= FatEntry.RESERVED.value or cluster < 2:
            return 0
        # FAT12 only
        # Align the offset to 3/2 ((cluster / 2) + cluster) because
        # two FAT entries are stored in 3 bytes (12 bits per FAT entry, 8 bits * 3 = 24 bits)
        offset = (cluster >> 0x0001) + cluster
        # Offset of the second fat relative to our FAT table
        offset_fat2 = (len(self.__fattabledata) >> 1) + offset

        # Get the previous value of the entire WORD.
        prev_fatent = int.from_bytes(self.__fattabledata[offset:offset+2], byteorder='little')

        # Encode the value depending on wether cluster is odd or even.
        # It's basically inverting the process of decoding, but we need to preserve the part of the other
        # value into our WORD. This is, the 4-bits that are on our word that belong to the other value.
        # If we are on an even cluster, we want to preserve the low 4-bits (nibble)
        # of the word and write our value into the 12 high bits.
        #
        # Being XYZ the value we are writting, abc and OPQ the previous values in the WORD):
        # bc Qa OP (little) -> OP Qa bc (BIG)
        # For even clusters (OP Qa), we want to preserve the low 4-bits:
        #   (0X YZ << 4 = XY Z0) | (OP Qa & 0x000F = 00 0a) = XY Za (XY Za bc)
        # For odd clusters (Qa bc), we want to preserve the low 4-bits of the previous
        # value of the word in the high 4-bits of the new word:
        #   (Qa bc << 12 = c0 00) | 0X YZ = cX YZ  (ab cX YZ)
        #
        # This is what I call a CLUSTERFUCK. If you have an existential crisis after reading this, you
        # are not alone.
        #
        # The last masking (& 0xFFFF) is just to ensure the result stays 4 bits because Python integers
        # can be as big as your memory.

        if cluster & 0x001 == 1:
            newval = ((value << 4) | (prev_fatent & 0x000F)) & 0xFFFF
        else:
            newval = ((prev_fatent << 12) | value)&0xFFFF

        # Write our changes to the FAT table
        newvalbytes = bytearray(newval.to_bytes(2, byteorder=sys.byteorder))
        self.__fattabledata[offset] = newvalbytes[0]
        self.__fattabledata[offset+1] = newvalbytes[1]
        # Also on the second FAT table
        self.__fattabledata[offset_fat2] = newvalbytes[0]
        self.__fattabledata[offset_fat2+1] = newvalbytes[1]
        # As a check, I will re-read the FAT and return it's value to ensure it's working correctly.
        return int.from_bytes(self.__fattabledata[offset:offset+2], byteorder='little')

    # (Private) Find the first free cluster available in the FAT. None if there are no available clusters.
    def __findFirstFreeCluster(self) -> int or None:
        for i in range(int(floor(len(self.__fattabledata)/2))):
            if self.__getNextCluster(i) == FatEntry.FREE.value:
                return i
        return None

    # (Private) Reserve the first free cluster available in the FAT. None if there are no available clusters.
    def __reserveFirstFreeCluster(self) -> int or None:
        for i in range(int(floor(len(self.__fattabledata)/2))):
            if self.__getNextCluster(i) == FatEntry.FREE.value:
                self.__updateCluster(i, FatEntry.LAST_CLUSTER)
                return i
        return None

    # (Private) (Unused) This basically checks if the cluster is not bad.
    # Should always return True (good) unless you use it on a physical drive or your physical
    # storage is actually bad. True = good, False = not good. DO NOT CALL THIS ON USED CLUSTERS
    def __chkCluster(self, cluster: int) -> bool:
        self.__fs.seek(self.__fileClusterToOffset(cluster), 0)
        self.__fs.write(bytearray([0xF6] * self.bootsector.bytesPerSector * self.bootsector.sectorsPerTrack))
        self.__fs.seek(self.__fileClusterToOffset(cluster), 0)
        buf_in = self.__fs.readandrewind(self.bootsector.bytesPerSector * self.bootsector.sectorsPerTrack, True)
        return not any((i != 0xF6) for i in buf_in)

    # (Private) (Unused) Read the FAT to obtain the offset of the next cluster
    def __getNextClusterOffset(self, cluster: int) -> int:
        return self.__fileClusterToOffset(self.__getNextCluster(cluster))

    # (Private) Builds the filesystem cache
    def __buildcache(self, startingCluster: int, curpath: str = "/"):
        current_cluster = startingCluster
        end = False
        clust = -1
        while not end:
            # Determine if this is the root directory or a regular directory.
            # The root directory has it's own space while regular directories are in the data section
            if int(current_cluster) != self.bootsector.rootDirCluster:
                # This is a regular directory, so we must obtain the offset from the data section and read
                # the entire cluster
                clust = current_cluster
                self.__fs.seek(self.__fileClusterToOffset(current_cluster), 0)
                buf_in = self.__fs.readandrewind(self.bootsector.bytesPerSector * self.bootsector.sectorsPerCluster)
            else:
                # This is the root directory. Seek there and read all of it's entries
                self.__fs.seek(self.bootsector.rootDirOffset)
                buf_in = self.__fs.readandrewind(self.bootsector.rootEntries * calcsize(DIRENTRY_FAT12_FORMAT))
            # Iterate through the data structures in the buffer
            i = 0 # direntry country
            for structbin in self.__dirstruct.iter_unpack(buf_in):
                dirent = Fat12DirectoryEntry(structbin)
                #dirent.debugPrint()
                #if dirent.firstLogicalCluster > 1:
                    #print(f"Content of cluster {dirent.firstLogicalCluster}:\n")
                    #self.__fs.printblock(self.__fileClusterToOffset(dirent.firstLogicalCluster), 512)
                if dirent.end:
                    # If this is the final entry, stop
                    end = True
                    break
                if dirent.free:
                    # If this file was removed (freed), skip it
                    continue
                elif dirent.invalid is not True:
                    # This is the volume label
                    if FATFileAttributes.LABEL in dirent.attributes:
                        self.vol_label = dirent.fileName
                        next
                    # This is a valid directory
                    elif FATFileAttributes.DIRECTORY in dirent.attributes:
                        # This entry corresponds to a subdirectory
                        if dirent.fileName != "." and dirent.fileName != "..":
                            # If this is not a special directory, scan the subdirectory (recursion)
                            self.__buildcache(dirent.firstLogicalCluster, str(curpath + dirent.fileName) if curpath == "/" else str(curpath + "/" + dirent.fileName))
                    # Append this directory entry to the cache, ignore special directories (. and ..)
                    if dirent.fileName != "." and dirent.fileName != ".." and FATFileAttributes.LABEL not in dirent.attributes:
                        # __fscache list: tuple(name (str), directory (str), full_path (str), is_directory (bool), direntry (FAT12DirectoryEntry), position (int), file cluster(int))
                        self.__fscache.append((f"{dirent.fileName.upper()}.{dirent.fileExtension.upper()}" if FATFileAttributes.DIRECTORY not in dirent.attributes and len(dirent.fileExtension) != 0 else dirent.fileName.upper(),
                                                os.path.dirname(curpath.upper()), curpath.upper(),
                                                (FATFileAttributes.DIRECTORY in dirent.attributes), dirent, i, clust))
                else:
                    pass # TODO: THROW AN INVALID DIRECTORY ENTRY EXCEPTION?
                i += 1 # Increase direntry counter
            # Check if we are done or not (if this is the root directory, we are done since it's fixed)
            if not end and current_cluster != self.bootsector.rootDirCluster:
                # Get the next cluster from the FAT
                current_cluster = self.__getNextCluster(current_cluster)
                clust = current_cluster
                # Check if the value is a bad cluster, a free cluster (it shouldn't), or if this is the
                # last cluster (and we are done)
                if current_cluster == FatEntry.BAD_CLUSTER.value:
                    break #  TODO: THROW IO EXCEPTION
                elif current_cluster == FatEntry.FREE.value:
                   break #  TODO: THROW FREE EXCEPTION
                elif current_cluster >= FatEntry.LAST_CLUSTER.value:
                    # This is the last cluster in the chain
                    end = True
                    break
        return

    # This function empties the cache and rebuilds it from scratch
    def __rebuildCache(self):
        self.__fscache = []
        self.__buildcache(self.bootsector.rootDirCluster, "/")

    # Search for a file or directory in the specified path
    def find(self, name: str, is_directory:bool = False, directory: str = "/") -> tuple or None:
        # Foolproof checks for the name and directory
        if name is None:
            return None
        if name == "":
            return None
        if directory is None:
            directory = "/"
        if directory == "":
            directory = "/"
        # is there a path in the name?
        if os.path.dirname(name) != "":
            # If so, we'll use it (will override whatever is in directory though)
            directory = os.path.dirname(name)
            name = os.path.basename(name)
        # Iterate through the cache
        for direntry in self.__fscache:
            # Check if the name matches. Names are not case sensitive in FAT12 (NOTE: Only if the case byte is not set (NT))
            if direntry[0].upper() == name.upper():
                # Check if the directory also matches
                if direntry[2].upper() == directory.upper():
                    # Are we searching for a file or a directory? If so, check if this is
                    # a file or a directory and return the result if it's a match.
                    if is_directory and FATFileAttributes.DIRECTORY in direntry[4].attributes:
                        return direntry
                    elif not is_directory and FATFileAttributes.DIRECTORY not in direntry[4].attributes:
                        return direntry
        # Not found
        return None

    # Search for any (file or directory) in the specified path.
    # This is the same as find but ignores if it's a directory or not.
    def findany(self, name: str, directory: str = "/") -> tuple or None:
        if name is None:
            return None
        if name == "":
            return None
        if directory is None:
            directory = "/"
        if directory == "":
            directory = "/"
        if os.path.dirname(name) != "":
            directory = os.path.dirname(name)
            name = os.path.basename(name)
        for direntry in self.__fscache:
            if direntry[0].upper() == name.upper():
                if direntry[2].upper() == directory.upper():
                    return direntry
        return None

    # Search any files or directories.
    # This will find all files and directories whose name matches the specified name.
    def findall(self, name: str) -> tuple or None:
        finds = []
        if name is None:
            return None
        if name == "":
            return None
        if os.path.dirname(name) != "":
            return None # Directory in findall exception
        for direntry in self.__fscache:
            if direntry[0].upper() == name.upper():
                finds.append(direntry)
        return tuple(finds)

    # (Private) Returns a tuple of Fat12DirectoryEntry instances with all the directory
    # entries for the directory starting at startingCluster.
    # TODO: NOT TESTED
    def __scandir(self, startingCluster: int) -> tuple:
        dirlist = []
        current_cluster = startingCluster
        end = False
        while not end:
            self.__fs.seek(self.__fileClusterToOffset(current_cluster), 0)
            buf_in = self.__fs.read(self.bootsector.bytesPerSector * self.bootsector.sectorsPerCluster)
            for structbin in self.__dirstruct.iter_unpack(buf_in):
                dirent = Fat12DirectoryEntry(structbin)
                if dirent.end:
                    # If this is the final entry, stop
                    end = True
                    break
                if dirent.free:
                    # If this file was removed (freed), skip it
                    next
                elif dirent.invalid is not True and dirent.fileName.isascii() and dirent.fileExtension.isascii():
                    dirlist.append(dirent)
            if not end:
                current_cluster = self.__getNextCluster(current_cluster)
                if current_cluster == FatEntry.BAD_CLUSTER.value:
                    break # TODO: THROW IO EXCEPTION
                elif current_cluster == FatEntry.FREE.value:
                   break # TODO: THROW FREE EXCEPTION
                elif current_cluster >= FatEntry.LAST_CLUSTER.value:
                    end = True
                    break
        return tuple(dirlist)   

    # Create an empty directory
    def mkdir(self, path: str) -> bool:
        dirname = os.path.basename(path)
        targetpath = os.path.split(path)[0]
        # Check if the directory exists
        if self.find(dirname,True,targetpath):
            return False # Directory already exists, can not create
        # Ensure we have at least one free cluster
        if self.__computeFreeClusters() < 1:
            return False # TODO: Out of space exception
        dirent = Fat12DirectoryEntry(None)
        dirent.fileName = dirname[0:8] # TODO: Long filename implementation?
        # pad name with spaces
        if len(dirent.fileName) < 8:
            dirent.fileName = dirent.fileName + (" " * (8 - len(dirent.fileName)))
        dirent.fileExtension = " " * 3
        dirent.attributes = [ FATFileAttributes.DIRECTORY ]
        dirent.case = 0
        dirent.creationTimeMS = FatUtils.getCurrent100thSecondByte()
        dirent.creationDate = FatUtils.encodeDate(self.global_time)
        dirent.creationTime = FatUtils.encodeTime(self.global_time)
        dirent.lastAccessDate = FatUtils.encodeDate(self.global_time)
        dirent.lastWriteDate = FatUtils.encodeDate(self.global_time)
        dirent.lastWriteTime = FatUtils.encodeTime(self.global_time)
        dirent.startingClusterHW = 0
        dirent.firstLogicalCluster = self.__reserveFirstFreeCluster()
        dirent.size = 0
        # Zero the cluster
        self.__fs.seek(self.__fileClusterToOffset(dirent.firstLogicalCluster), 0)
        self.__fs.write(bytearray(self.bootsector.bytesPerSector * self.bootsector.sectorsPerCluster))
        # Copy the directory entry. This also updates the cache
        return self.__copyDirectoryEntry(dirent, targetpath)

    # (private) Finds the logical (byte) offset of the directory entry for a specified file/folder
    # Returns -1 if not found
    def __getDirEntryOffset(self, direntry: Fat12DirectoryEntry, path: str = "/") -> int:
        if path == "/":
            # Seek to the start of the root
            self.__fs.seek(self.bootsector.rootDirOffset, 0)
            # Read the root directory
            buf_in = self.__fs.read(self.bootsector.rootEntries * calcsize(DIRENTRY_FAT12_FORMAT))
            i = 0 # direntry counter
            # Iterate each directory entry
            for dirstruct in self.__dirstruct.iter_unpack(buf_in):
                curdir = Fat12DirectoryEntry(dirstruct)
                if curdir.fileName == direntry.fileName:
                    return self.bootsector.rootDirOffset + (i * calcsize(DIRENTRY_FAT12_FORMAT))
                i = i + 1
        else:
            # Find the directory entry in the cache
            directory = self.find(os.path.basename(path), True, os.path.split(path)[0])
            if not directory:
                return False
            # Get the first logical cluster of the directory entry
            cur_cluster = directory[4].firstLogicalCluster
            if cur_cluster == 0:
                # This folder is probably empty
                return -1
            next_cluster = cur_cluster
            # Read all the clusters of this directory
            while next_cluster != FatEntry.LAST_CLUSTER.value and next_cluster != FatEntry.BAD_CLUSTER.value:
                # Read the cluster
                self.__fs.seek(self.__fileClusterToOffset(next_cluster), 0)
                buf_in = self.__fs.read(self.bootsector.bytesPerSector * self.bootsector.sectorsPerCluster)
                i = 0 # Direntry counter
                # Iterate through directory entries in the current cluster
                for dirstruct in self.__dirstruct.iter_unpack(buf_in):
                    # Read the directory entry
                    curdir = Fat12DirectoryEntry(dirstruct)
                    if curdir.fileName == direntry.fileName:
                        return self.__fileClusterToOffset(next_cluster) + (calcsize(DIRENTRY_FAT12_FORMAT) * i)
                    i += 1
                # Get the next cluster
                next_cluster = self.__getNextCluster(cur_cluster)
                cur_cluster = next_cluster
            return -1

    # (Private) Copy a directory entry into the first free directory entry of the specified path
    # This is almost spaghetti code, I know. Sorry about the messiness. TODO: Improve it
    def __copyDirectoryEntry(self, direntry: Fat12DirectoryEntry, path: str = "/") -> bool:
        # If the path is root
        if path == "/":
            # Seek to the start of the root
            self.__fs.seek(self.bootsector.rootDirOffset, 0)
            # Read the root directory
            buf_in = self.__fs.read(self.bootsector.rootEntries * calcsize(DIRENTRY_FAT12_FORMAT))
            i = 0 # direntry counter
            # Iterate each directory entry
            for dirstruct in self.__dirstruct.iter_unpack(buf_in):
                # Read the directory entry
                curdir = Fat12DirectoryEntry(dirstruct)
                # Is it free?
                if curdir.free:
                    # Use it
                    self.__fs.seek(self.bootsector.rootDirOffset + (calcsize(DIRENTRY_FAT12_FORMAT) * i), 0)
                    dtup = direntry.getDataTuple()
                    self.__fs.write(bytearray(self.__dirstruct.pack(*dtup)))
                    # Rebuild cache
                    self.__rebuildCache()
                    return True
                # Is this the last entry?
                elif curdir.end:
                    # Do we have space beyond this entry?
                    if i < self.bootsector.rootEntries:
                        # Use this entry
                        self.__fs.seek(self.bootsector.rootDirOffset + (calcsize(DIRENTRY_FAT12_FORMAT) * i), 0)
                        dtup = direntry.getDataTuple()
                        self.__fs.write(bytearray(self.__dirstruct.pack(*dtup)))
                        # Set the next entry as the new directory end
                        self.__fs.seek(self.bootsector.rootDirOffset + (calcsize(DIRENTRY_FAT12_FORMAT) * (i+1)), 0)
                        self.__fs.write(bytearray([DirEntryClass.FINAL.value]))
                        # Rebuild cache
                        self.__rebuildCache()
                        return True
                    else:
                        return False # TODO: Out of directory space
                elif curdir.invalid:
                    pass #TODO: Throw exception here
                i += 1
            return False
        # If the path is not root
        else:
            # Find the directory entry in the cache
            directory = self.find(os.path.basename(path), True, os.path.split(path)[0])
            if not directory:
                return False
            # Get the first logical cluster of the directory entry
            cur_cluster = directory[4].firstLogicalCluster

            # Is the first logical cluster not assigned? (Empty directory):
            if directory[4].firstLogicalCluster == 0:
                # We will need to allocate a new cluster. Request a free cluster:
                directory[4].firstLogicalCluster = self.__reserveFirstFreeCluster()
                if not directory[4].firstLogicalCluster:
                    return False # TODO: Throw out of free space exception here
                # Get the tuple with the direntry structure
                dtup = directory[4].getDataTuple()
                # Get the parent directory direntry from the cache:
                if directory[1] != "/":
                    # If this is not root, look in the cache
                    parent = self.find(directory[1], True, directory[2])
                    # Get the directory entry index inside of the cluster
                    findex = directory[5]
                    prevdircluster = parent[6]
                    # Write changes to disc
                    self.__fs.seek(self.__fileClusterToOffset(parent[6]) + ((calcsize(DIRENTRY_FAT12_FORMAT) * findex)))
                    self.__fs.write(bytearray(self.__dirstruct.pack(*dtup)))
                else:
                    # Write changes to the root directory
                    self.__fs.seek(self.bootsector.rootDirOffset + (calcsize(DIRENTRY_FAT12_FORMAT) * directory[5]), 0)
                    self.__fs.write(bytearray(self.__dirstruct.pack(*dtup)))
                    prevdircluster = 0
                # Get the special directories
                curdir = FatUtils.getCurrentDirectory(directory[4].firstLogicalCluster, self.global_time)
                prevdir = FatUtils.getParentDirectory(prevdircluster, self.global_time)

                # Write the special directories
                self.__fs.seek(self.__fileClusterToOffset(directory[4].firstLogicalCluster), 0)

                # Zero this cluster
                self.__fs.zero(self.bootsector.bytesPerSector * self.bootsector.sectorsPerCluster)
                self.__fs.write(bytearray(self.__dirstruct.pack(*curdir.getDataTuple())))
                self.__fs.write(bytearray(self.__dirstruct.pack(*prevdir.getDataTuple())))

                # And then write the directory entry
                self.__fs.write(bytearray(self.__dirstruct.pack(*direntry.getDataTuple())))

                # Rebuild cache
                self.__rebuildCache()
                return True

            # First logical cluster was asigned already. Iterate through clusters.
            next_cluster = cur_cluster
            while next_cluster != FatEntry.LAST_CLUSTER.value and next_cluster != FatEntry.BAD_CLUSTER.value:
                # Read the cluster
                self.__fs.seek(self.__fileClusterToOffset(next_cluster), 0)
                buf_in = self.__fs.read(self.bootsector.bytesPerSector * self.bootsector.sectorsPerCluster)
                i = 0 # Direntry counter
                # Iterate through directory entries in the current cluster
                for dirstruct in self.__dirstruct.iter_unpack(buf_in):
                    # Read the directory entry
                    curdir = Fat12DirectoryEntry(dirstruct)
                    # Is it free?
                    if curdir.free:
                        # Use it
                        self.__fs.seek(self.__fileClusterToOffset(next_cluster) + (calcsize(DIRENTRY_FAT12_FORMAT) * i), 0)
                        self.__fs.write(bytearray(self.__dirstruct.pack(*direntry.getDataTuple())))
                        # Rebuild cache
                        self.__rebuildCache()
                        return True
                    # Is this the end of the directory?
                    elif curdir.end:
                        # Are there any entries left in the cluster?
                        if i < 15:
                            # Use this space and mark the next entry as the end
                            self.__fs.seek(self.__fileClusterToOffset(next_cluster) + (calcsize(DIRENTRY_FAT12_FORMAT) * i), 0)
                            self.__fs.write(bytearray(self.__dirstruct.pack(*direntry.getDataTuple())))
                            self.__fs.seek(self.__fileClusterToOffset(next_cluster) + (calcsize(DIRENTRY_FAT12_FORMAT) * (i+1)), 0)
                            self.__fs.write(bytearray([DirEntryClass.FINAL.value]))
                            # Rebuild cache
                            self.__rebuildCache()
                            return True
                        else:
                            # We'll need a new cluster, request one:
                            freec = self.__reserveFirstFreeCluster()
                            if not freec:
                                return False # TODO: Throw out of free space exception here
                            # Update the next cluster
                            self.__updateCluster(cur_cluster, freec)

                            # Use this space and mark the next entry (in the next cluster) as the end
                            self.__fs.seek(self.__fileClusterToOffset(next_cluster) + (calcsize(DIRENTRY_FAT12_FORMAT) * i), 0)
                            self.__fs.write(bytearray(self.__dirstruct.pack(*direntry.getDataTuple())))
                            self.__fs.seek(self.__fileClusterToOffset(freec), 0)
                            self.__fs.write(bytearray([DirEntryClass.FINAL.value]))
                            # Rebuild cache
                            self.__rebuildCache()
                            return True
                    elif curdir.invalid:
                        pass #TODO: Throw exception here, or maybe use it
                    i += 1 # Increase direntry counter
                # Get the next cluster
                next_cluster = self.__getNextCluster(cur_cluster)
                cur_cluster = next_cluster
            return False


    # (Private) Returns a tuple of Fat12DirectoryEntry instances with all the directory
    # entries for the root directory.
    def __getRootDirectory(self) -> tuple:
        dirlist = []
        # The root directory has a fixed size. No need to iterate through clusters.
        self.__fs.seek(self.bootsector.rootDirOffset)
        buf_in = self.__fs.readandrewind(self.bootsector.rootEntries * calcsize(DIRENTRY_FAT12_FORMAT))
        for structbin in self.__dirstruct.iter_unpack(buf_in):
            dirent = Fat12DirectoryEntry(structbin)
            if dirent.end:
                # If this is the final entry, stop
                break
            if dirent.free:
                # If this file was removed (freed), skip it
                continue
            # If the directory entry is valid, append it to the list
            elif dirent.invalid is not True: dirlist.append(dirent)
        return tuple(dirlist)

    # Extracts a file from the file system
    def extract(self, path: str, output: str) -> bool:
        # Find the file in the filesystem first.
        file = self.find(path, False)
        if file is None:
            return False # TODO: Maybe throw a file not found exception?
        try:
            # Seek to the first logical cluster of the file
            self.__fs.seek(self.__fileClusterToOffset(file[4].firstLogicalCluster), 0)
            # Create a FileHandler for the output file
            with FileHandler(output, FileMode.WRITEBYTES, True) as of:
                # Is this the last cluster?
                final = False
                # Bytes left to write (will substract from this when writing)
                to_write = file[4].size
                # The next logical cluster
                nextLogicalCluster = file[4].firstLogicalCluster
                # Start the extraction
                while not final and to_write > 0:
                    # How many bytes are we trying to read/write
                    writex = self.bootsector.bytesPerSector * self.bootsector.sectorsPerCluster
                    # If we are trying to read more bytes than available, adjust it
                    if to_write < writex:
                        writex = to_write
                    # Read from the file system
                    buf_in = self.__fs.read(writex)
                    # Write to the output
                    wd = of.write(buf_in)
                    # Substract written bytes from our counter
                    to_write -= wd
                    # Get the next cluster from the FAT
                    nextLogicalCluster = self.__getNextCluster(nextLogicalCluster)
                    # If this is the last cluster (or it's reserved), we are done.
                    if nextLogicalCluster >= FatEntry.LAST_CLUSTER.value or (nextLogicalCluster >= FatEntry.RESERVED.value and nextLogicalCluster <= FatEntry.RESERVED2.value):
                        final = True
                        break
                    # If this is a bad cluster, run in circles and panic
                    elif nextLogicalCluster == FatEntry.BAD_CLUSTER.value:
                        final = True
                        # Throw BAD CLUSTER exception or something
                        return False
                    # Otherwise, seek to the offset of the next cluster
                    if not final:
                        self.__fs.seek(self.__fileClusterToOffset(nextLogicalCluster), 0)
            # We are done
            return True
        except Exception as exc:
            raise exc

    # (private) Get the number of available clusters
    def __computeFreeClusters(self) -> int:
        free = 0
        # Walk through the FAT
        for i in range(self.bootsector.fatEntries):
            # If this cluster is free, add 1 to the counter
            free = (free + 1) if self.__getNextCluster(i) == FatEntry.FREE.value else free
        return free

    # (public) Get the amount of available space (in bytes or clusters)
    def getFreeSpace(self, clusters = False) -> int:
        if not clusters:
            return self.__computeFreeClusters() * self.bootsector.bytesPerSector * self.bootsector.sectorsPerCluster
        else:
            return self.__computeFreeClusters()

    # Commit changes to the FAT on the filesystem
    def updateFatFS(self):
        self.__fs.seek(self.bootsector.bytesPerSector,0)
        i = self.__fs.write(self.__fattabledata)

    # Writes a file to the FS
    def writeFile(self, file: str, path: str = "/", overwrite: bool = False) -> bool:
        # Check if the file already exists
        if self.find(file, False, path) and not overwrite:
            return False # File already exists. TODO: Throw an exception here
        
        with FileHandler(file, FileMode.READBYTES, False) as f:
            # How many clusters do we need?
            clusters = int(ceil(f.file_size / (self.bootsector.bytesPerSector * self.bootsector.sectorsPerCluster)))

            # Do we have enough space?
            if self.__computeFreeClusters() < clusters:
                return False # Out of space TODO: Throw an exception here

            # Find the first available cluster
            freec = self.__reserveFirstFreeCluster()

            # No available clusters? No file for you. >:(
            if freec is None:
                return False

            # Create a new directory entry
            dirent = Fat12DirectoryEntry(None)

            # Fill the directory entry
            dirent.fileName = f.filename[0:8]
            # pad name with spaces
            if len(dirent.fileName) < 8:
                dirent.fileName = dirent.fileName + (" " * (8 - len(dirent.fileName)))

            dirent.fileExtension = f.extension
            # pad extension with spaces
            if len(dirent.fileName) < 3:
                dirent.fileName = dirent.fileName + (" " * (3 - len(dirent.fileExtension)))
            # Fill the rest of the structure
            dirent.attributes = [] # TODO: File attributes should go here (Asuming none for now)
            dirent.case = 0
            dirent.creationTimeMS = FatUtils.getCurrent100thSecondByte()
            dirent.creationDate = FatUtils.encodeDate(self.global_time)
            dirent.creationTime = FatUtils.encodeTime(self.global_time)
            dirent.lastAccessDate = FatUtils.encodeDate(self.global_time)
            dirent.lastWriteDate = FatUtils.encodeDate(self.global_time)
            dirent.lastWriteTime = FatUtils.encodeTime(self.global_time)
            dirent.startingClusterHW = 0
            dirent.firstLogicalCluster = freec if clusters > 0 else 0
            dirent.size = f.file_size if clusters > 0 else 0

            # Write the directory entry
            if not self.__copyDirectoryEntry(dirent, path):
                return False

            # Begin writting in the data area
            while clusters > 0:
                if freec is not None:
                    # Update the FAT table to LAST_CLUSTER (we'll update it again later if this is not the case)
                    self.__updateCluster(freec, FatEntry.LAST_CLUSTER)

                    # Write the data
                    cluster = f.read(self.bootsector.bytesPerSector * self.bootsector.sectorsPerCluster, True)
                    fsk = self.__fs.seek(self.__fileClusterToOffset(freec),0)
                    self.__fs.write(cluster)

                    # Substract cluster
                    clusters -= 1

                    # Is this the last cluster?
                    if clusters > 0:
                        # Find another free cluster
                        freec2 = self.__reserveFirstFreeCluster()

                        # No more clusters available? Oof! That's not good.
                        if freec2 is None:
                            return False
                    else:
                        # This is the last cluster
                        freec2 = FatEntry.LAST_CLUSTER

                    # Update the FAT table (This time for good).
                    if freec != FatEntry.LAST_CLUSTER:
                        self.__updateCluster(freec, freec2)
                    else:
                        # There is nothing left to do here
                        break
                    # The next loop will use the next cluster
                    freec = freec2
                else:
                    # if this is not a free cluster something went terribly wrong
                    return False

        # Commit changes to the FAT
        self.updateFatFS()

        # Rebuild the fs cache
        self.__rebuildCache()
        return True

    # Delete a file or directory from the file system
    def delFile(self, file: str, path: str = "/", isDirectory: bool = False) -> bool:
        if file == "." or file == "..":
            return False
        file_ent = self.find(file, isDirectory, path)
        if not file_ent:
            return False
        direntry = file_ent[4]
        i = 0
        if file:
            if isDirectory:
                # Scan this directory:
                dirtree = self.__scandir(direntry.firstLogicalCluster)
                # Recursively delete everything:
                for direntryd in dirtree:
                    if direntryd.fileName == "." or direntryd.fileName == "..":
                        next
                    nextpath = (path + "/" + file) if path != "/" else ("/" + file)
                    if FATFileAttributes.DIRECTORY in direntryd.attributes:
                        self.delFile(direntryd.fileName, nextpath, True)
                    else:
                        self.delFile(f"{direntryd.fileName}.{direntryd.fileExtension}", nextpath, False)
            # Scan all clusters used by this file and set them free
            i = direntry.firstLogicalCluster
            clusters = []
            # Definitely not the most elegant way to do this but this was giving me issues and I am tired. If it works it works.
            while i < FatEntry.LAST_CLUSTER.value and i > 1: # TODO: Might lead to errors
                clusters.append(i)
                i = self.__getNextCluster(i)
            for cluster in clusters:
                self.__updateCluster(cluster, FatEntry.FREE.value)
            # Commit changes to the FAT
            self.updateFatFS()
            # Mark this entry as free
            direntryoffset = self.__getDirEntryOffset(direntry, path)
            if direntryoffset < 0:
                return False # TODO: Throw exception here
            self.__fs.seek(direntryoffset, 0)
            x = self.__fs.write(bytearray([DirEntryClass.FREE.value]))
            # Rebuild the cache
            self.__rebuildCache()
            return True

        else:
            return False # File does not exists

    # Move a file from one directory to another
    def moveFile(self, file: str, targetpath: str, path: str = "/") -> bool:
        # Foolproof checks
        if file == "." or file == "..":
            return False
        # Find the original file
        file_ent = self.find(file, False, path)
        if not file_ent:
            return False
        # Get the directory entry
        direntry = file_ent[4]
        # Copy the directory entry to the target path
        if self.__copyDirectoryEntry(direntry, targetpath):
            # Free the original directory entry
            i = self.__getDirEntryOffset(direntry, path)
            self.__fs.seek(i, 0)
            self.__fs.write(bytearray([DirEntryClass.FREE.value]))
            return True
        else:
            return False

    # Copy a file from one directory to another
    def copyFile(self, file: str, targetpath: str, path: str = "/", isDir = False) -> bool:
        # Foolproof checks
        if file == "." or file == "..":
            return False
        # Find the original file
        file_ent = self.find(file, isDir, path)
        if not file_ent:
            return False
        # Get the directory entry
        direntry = file_ent[4]
        # Copy the clusters
        i = direntry.firstLogicalCluster
        clusters = []
        # Definitely not the most elegant way to do this but this was giving me issues and I am tired. If it works it works.
        while i < FatEntry.RESERVED.value and i > 1: # TODO: Might lead to errors
            clusters.append(i)
            i = self.__getNextCluster(i)
        # Do we have enough clusters available?
        if len(clusters) > self.__computeFreeClusters():
            return False # Nope
        # Copy file clusters
        i = -1
        for cluster in clusters:
            # If this is the first cluster, reserve one cluster and update the directory entry.
            if i == -1:
                i = self.__reserveFirstFreeCluster()
                direntry.firstLogicalCluster = i
            else:
                # Reserve a cluster and set the next cluster in the chain
                k = i
                i = self.__reserveFirstFreeCluster()
                self.__updateCluster(k,i)
            # Read the data from the original cluster
            self.__fs.seek(self.__fileClusterToOffset(cluster),0)
            buf_in = self.__fs.read(self.bootsector.bytesPerSector * self.bootsector.sectorsPerCluster, False)
            # Copy the data to the destination
            self.__fs.seek(self.__fileClusterToOffset(i),0)
            self.__fs.write(buf_in)
        # Update the last cluster
        self.__updateCluster(i, FatEntry.LAST_CLUSTER.value)
        # Commit changes to the FAT
        self.updateFatFS()
        # Copy the directory entry to the target path
        if self.__copyDirectoryEntry(direntry, targetpath):
            return True
        else:
            return False

    # Rename a file
    def rename(self, file_name: str, newname: str, path: str = "/", isDirectory: bool = False) -> bool:
        # Foolproof checks
        if file_name == "." or file_name == "..":
            return False
        # Find the original file
        file_ent = self.find(file_name, isDirectory, path)
        if not file_ent:
            return False
        # Get the directory entry
        direntry = file_ent[4]
        dirEntryOffset = self.__getDirEntryOffset(direntry, path)
        newext = "   "
        # Get name . ext
        if "." in newname:
            namebits = newname.split(".")
            newname = namebits[0]
            newext = namebits[1]
        # Trim the new name to 8 characters maximum (pad with spaces if not)
        if len(newname) > 8:
            newname = newname[0:7] + "~"
        elif len(newname) < 8:
            newname = newname + (" " * (8-len(newname)))
        # Same but with the extension
        if len(newext) > 3:
            newext = newext[0:3]
        elif len(newext) < 3:
            newext = newext + (" " * (3-len(newname)))
        # Is the new name valid?
        if not FatUtils.isValidDirName(newname):
            return False # Nope
        if not FatUtils.isValidDirName(newext):
            return False # Nope!
        # Set the new name
        direntry.fileName = newname
        direntry.fileExtension = newext
        # Update this directory entry
        self.__fs.seek(dirEntryOffset, 0)
        self.__fs.write(bytearray(self.__dirstruct.pack(*direntry.getDataTuple())))
        return True

    # Sets the global time (used for files and folders)
    def setGlobalTime(self, date: datetime = None, day: int = 1, month: int = 1, year: int = 1990, hour: int = 0, minute: int = 0, second: int = 0, msecond: int = 1):
        if date is None:
            self.global_time = datetime(year,month,day,hour,minute,second,msecond)
        else:
            self.global_time = date

    # Returns the filesystem cache
    def fscache(self) -> tuple:
        return tuple(self.__fscache)

    # Closes the filesystem
    def closefs(self):
        self.__fs.close()