# Test program for FatFS.py.

from FatFS import FatFS, FloppyTypes

if __name__ == "__main__":

        # Create a new Floppy image (1.44MB)
        fs = FatFS("testfat/new.img",True, FloppyTypes.FLOP144M)
        # Change current date
        fs.setGlobalTime(None,8,1,1990,13,20,0,15)
        # Write a few files in it (To the root directory)
        if not fs.writeFile("./testfat/copy/TEST.GIF"):
                print("WRITE OPERATION FAILED FOR FILE TEST.GIF")
        if not fs.writeFile("./testfat/copy/TEST.TXT"):
                print("WRITE OPERATION FAILED FOR FILE TEST.TXT")
        # Make a new directory
        if not fs.mkdir("/TEST"):
                print("WRITE OPERATION FAILED FOR DIRECTORY /TEST")
        # Copy another file in the new directory
        if not fs.writeFile("./testfat/copy/TEST2.GIF", "/TEST"):
                print("WRITE OPERATION FAILED FOR FILE TEST2.GIF")
        # Make a subdirectory
        if not fs.mkdir("/TEST/SUBDIR"):
                print("WRITE OPERATION FAILED FOR DIRECTORY /TEST/SUBDIR")
        # Copy another file in the new subdirectory
        if not fs.writeFile("./testfat/copy/TEST3.GIF", "/TEST/SUBDIR"):
                print("WRITE OPERATION FAILED FOR FILE TEST3.GIF")
        # Copy TEST3.GIF to the root directory
        if not fs.copyFile("TEST3.GIF", "/", "/TEST/SUBDIR"):
                print("COPY OPERATION FAILED FOR TEST3.GIF")
        # Move TEST2.GIF from /TEST to /TEST/SUBDIR
        if not fs.moveFile("TEST2.GIF", "/TEST/SUBDIR", "/TEST"):
                print("MOVE OPERATION FAILED FOR TEST2.GIF")
        # Delete TEST3.GIF from /TEST/SUBDIR
        if not fs.delFile("TEST3.GIF", "/TEST/SUBDIR"):
                print("DELETE OPERATION FAILED FOR FILE TEST3.GIF IN /TEST/SUBDIR")
        # Close the media
        fs.closefs()
